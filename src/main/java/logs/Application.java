package logs;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Application {
    private static final Logger loggerFI = LogManager.getLogger(Application.class);

    public static void main(final String[] args) {
        loggerFI.trace("Just test!");
        loggerFI.trace("Entering application.");
        Bar bar = new Bar();
        loggerFI.debug(" before !bar.doIt");
        if (!bar.doIt()) {
            loggerFI.debug("Testing logger.");
            loggerFI.error("Didn't do it.");
            loggerFI.fatal("Testing sms.");
        }
        loggerFI.info("Before exiting application.");
        loggerFI.trace("Exiting application.");
    }
}

class Bar {
    static final Logger loggerPI = LogManager.getLogger(Bar.class.getName());

    public boolean doIt() {
        loggerPI.trace("Enter Bar.");
        loggerPI.error("Did it again!");
        return false;
    }
}
