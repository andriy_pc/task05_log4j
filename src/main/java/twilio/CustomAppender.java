package twilio;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.*;

import java.io.Serializable;

@Plugin(name="SMS", category="Core", elementType="appender", printObject=true)
public final class CustomAppender extends AbstractAppender {
    protected CustomAppender(String name, Filter filter,
                             Layout<? extends Serializable> layout,
                             final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }
    @PluginFactory
    public static CustomAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new CustomAppender(name, filter, layout, true);
    }

    @Override
    public void append(LogEvent event) {
        try {
            ExampleSMS.send(new String(getLayout().toByteArray(event)));
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
    /*@Override
    public void append(LogEvent event) {
        try {
            ExampleSMS.send(new String(getLayout().toByteArray(event)));
        } catch (Exception e){
            e.printStackTrace(System.out);
        }
    }*/
}
