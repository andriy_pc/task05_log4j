package twilio;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACd5eee858528b2dfd4ad898c6c39528c9";
    public static final String AUTH_TOKEN = "bea97937694b1edebba1dd604a74535c";

    public static void send(String msg) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380957724634"),
                new PhoneNumber("+17743103582"),
                msg).create();
    }
}
